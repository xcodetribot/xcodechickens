package scripts.xCodeChickens;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.DynamicMouse;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.CustomRet_0P;
import org.tribot.api2007.Game;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.Projection;
import org.tribot.api2007.types.RSNPC;

public class HoverOverNextChicken extends Node{

	@Override
	public boolean isValid() {
		return NPCs.find("Chicken").length > 0 && Player.getRSPlayer().isInCombat() && !Game.getUptext().contains("Attack");
	}

	@Override
	public void execute() {
		RSNPC[] chickens = NPCs.findNearest("Chicken");
		
		for(RSNPC chicken : chickens)
		{
			if(chicken.isInteractingWithMe() || chicken.isInCombat() || !chickenArea.contains(chicken.getPosition()) || !chicken.isOnScreen() || xCodeChickens.AntiBan.BOOL_TRACKER.USE_CLOSEST.next())
				continue;
			
			if(xCodeChickens.AntiBan.BOOL_TRACKER.HOVER_NEXT.next())
				chicken.hover();
			
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					return Game.getUptext().contains("Attack");
				}
			}, General.random(2000, 2500));
			break;
		}
	}

}
