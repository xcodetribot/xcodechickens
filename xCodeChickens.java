package scripts.xCodeChickens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.EventBlockingOverride;
import org.tribot.script.interfaces.Painting;

@ScriptManifest(authors={"xCode"}, category="Combat", name="xCodeChickens", version = 1.1, description="Start the bot at the chickens south of Falador. <br>Choose if you want to pick up feathers and bury bones.")
public class xCodeChickens extends Script implements Painting, EventBlockingOverride{

	private ArrayList<Node> nodes = new ArrayList<Node>();
	private String status = "";
	private boolean stop = false,
					pickupFeathers = false,
					pickupburyBones = false,
					showPaint = true;
	private int feathersAtStart = 0,
				startXPStrength = 0,
				startLevelStrength = 0,
				startXPAttack = 0,
				startLevelAttack = 0,
				startXPDefence = 0,
				startLevelDefence = 0;
	private final long startTime = System.currentTimeMillis();
	private final RenderingHints ANTIALIASING = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	private final Image 	paint = getImage("http://oi60.tinypic.com/257oqr9.jpg"),
					paintDisabled = getImage("http://i60.tinypic.com/whxzrn.png");
	private final Rectangle toggleArea = new Rectangle(478, 340, 28, 29);
	
	public static final ABCUtil AntiBan = new ABCUtil();
	
	public void run() {
		initialize();
		
		while(!stop)
		{
			for (final Node n : nodes) {
				if (n.isValid()) {
					status = n.toString();
					n.execute();
				}
			}
			sleep(General.random(10, 50));
		}
	}

	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D) gg;
		g.setRenderingHints(ANTIALIASING);
		
		if(showPaint)
		{
			g.drawImage(paint, 0, 338, null);
			Font font = new Font("Arial", Font.BOLD, 14);
			g.setFont(font);
			g.setColor(Color.white);
			
			g.drawString("Attack XP (Level): " + (Skills.getXP(SKILLS.ATTACK) - startXPAttack) + " (" + Skills.getActualLevel(SKILLS.ATTACK) + "+" + (Skills.getActualLevel(SKILLS.ATTACK) - startLevelAttack) + ")", 160, 390);
			g.drawString("Strength XP (Level): " + (Skills.getXP(SKILLS.STRENGTH) - startXPStrength) + " (" + Skills.getActualLevel(SKILLS.STRENGTH) + "+" + (Skills.getActualLevel(SKILLS.STRENGTH) - startLevelStrength) + ")", 160, 410);
			g.drawString("Defence XP (Level): " + (Skills.getXP(SKILLS.DEFENCE) - startXPDefence) + " (" + Skills.getActualLevel(SKILLS.DEFENCE) + "+" + (Skills.getActualLevel(SKILLS.DEFENCE) - startLevelDefence) + ")", 160, 430);
		
			g.drawString("Runtime: " + Timing.msToString(Timing.timeFromMark(startTime)), 160, 470);
		
			RSItem[] feathers = Inventory.find("Feather");
			int feathersGained = 0;
			if(feathers.length > 0)
				feathersGained = feathers[0].getStack() - feathersAtStart;
			g.drawString("Feathers: " + feathersGained, 300, 470);
		}
		else
			g.drawImage(paintDisabled, 0, 338, null);
	}
	
	/*
	 * Creates 2 Dialogs which will setup picking up feathers & bones, and to bury them.
	 */
	private void initialize()
	{
		int boneDialogResult = JOptionPane.showConfirmDialog (null, "Would you like to pick up and bury bones?","Bones",JOptionPane.YES_NO_OPTION);
		int featherDialogResult = JOptionPane.showConfirmDialog (null, "Would you like to pick up feathers?","Feathers",JOptionPane.YES_NO_OPTION);
		pickupburyBones = boneDialogResult == JOptionPane.YES_OPTION;
		pickupFeathers = featherDialogResult == JOptionPane.YES_OPTION;
		
		println("Pick up and bury bones: " + pickupburyBones);
		println("Pick up feathers: " + pickupFeathers);
		
		nodes.add(new WalkToChickens());
		nodes.add((new PickUpItems(pickupFeathers, pickupburyBones)));
		nodes.add(new AttackChicken());
		nodes.add(new HoverOverNextChicken());
		if(pickupburyBones)
			nodes.add(new BuryBones());
		nodes.add(new DropJunk());
		nodes.add(new Antiban());
		
		//Set start variables.
		RSItem[] feathers = Inventory.find("Feather");
		if(feathers.length == 0)
			feathersAtStart = 0;
		else
			feathersAtStart = feathers[0].getStack();
		
		startLevelStrength = Skills.getActualLevel(SKILLS.STRENGTH);
		startXPStrength = Skills.getXP(SKILLS.STRENGTH);
		
		startLevelAttack = Skills.getActualLevel(SKILLS.ATTACK);
		startXPAttack = Skills.getXP(SKILLS.ATTACK);
		
		startLevelDefence = Skills.getActualLevel(SKILLS.DEFENCE);
		startXPDefence = Skills.getXP(SKILLS.DEFENCE);
		
		Mouse.setSpeed(General.random(120, 130));
	}
	
	/***
	 * Return image based on URL.
	 * @param url
	 * @return
	 */
	private Image getImage(String url) {
		try {
			return ImageIO.read(new URL(url));
		} catch (IOException e) {
			return null;
		}
	}

	public OVERRIDE_RETURN overrideKeyEvent(KeyEvent arg0) {
		return OVERRIDE_RETURN.SEND;
	}

	public OVERRIDE_RETURN overrideMouseEvent(MouseEvent e) {
		try
		{
		    if (toggleArea.contains(e.getPoint()))
		    {
				if (e.getID() == MouseEvent.MOUSE_CLICKED)
				{
				    e.consume();
				    if (!showPaint)
				    	showPaint = true;
				    else
				    	showPaint = false;
				    return OVERRIDE_RETURN.DISMISS;
				} else if (e.getID() == MouseEvent.MOUSE_PRESSED)
				    return OVERRIDE_RETURN.DISMISS;
		    }
		    return OVERRIDE_RETURN.PROCESS;

		} catch (Exception e2)
		{
		    return OVERRIDE_RETURN.DISMISS;
		}
	}

}
