package scripts.xCodeChickens;

import org.tribot.api2007.Inventory;

public class DropJunk extends Node{

	@Override
	public boolean isValid() {
		return Inventory.isFull();
	}

	@Override
	public void execute() {
		Inventory.dropAllExcept(new String[] {"Feather"});		
	}

}
