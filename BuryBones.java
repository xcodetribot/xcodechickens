package scripts.xCodeChickens;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSItem;

public class BuryBones extends Node{

	@Override
	public boolean isValid() {
		return Inventory.getCount("Bones") > 0 && !Player.getRSPlayer().isInCombat();
	}

	@Override
	public void execute() {
		RSItem[] bones = Inventory.find("Bones");
		
		for(RSItem bone : bones)
		{
			bone.click("Bury");
			General.sleep(xCodeChickens.AntiBan.DELAY_TRACKER.ITEM_INTERACTION.next());
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					return Player.getAnimation() == -1;
				}
			}, General.random(800, 1000));
		}
	}

}
