package scripts.xCodeChickens;

import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;

import scripts.xCodeChickens.xCodeChickens;

public class Antiban extends Node{
	@Override
	public boolean isValid() {
		return Player.getAnimation() != -1 || Player.isMoving();
	}

	@Override
	public void execute() {
		
		xCodeChickens.AntiBan.performCombatCheck();

		xCodeChickens.AntiBan.performEquipmentCheck();

		xCodeChickens.AntiBan.performExamineObject();

		xCodeChickens.AntiBan.performFriendsCheck();

		xCodeChickens.AntiBan.performLeaveGame();

		xCodeChickens.AntiBan.performMusicCheck();

		xCodeChickens.AntiBan.performPickupMouse();

		xCodeChickens.AntiBan.performQuestsCheck();

		xCodeChickens.AntiBan.performRandomMouseMovement();

		xCodeChickens.AntiBan.performRandomRightClick();

		xCodeChickens.AntiBan.performRotateCamera();

		SKILLS combatType;
		switch(Combat.getSelectedStyleIndex())
		{
		case 0:
			combatType = SKILLS.ATTACK;
			break;
		case 1:
			combatType = SKILLS.STRENGTH;
			break;
		case 3:
			combatType = SKILLS.DEFENCE;
			break;
		default:
			combatType = SKILLS.STRENGTH;
			break;
		}
		
		xCodeChickens.AntiBan.performTimedActions(combatType);

		xCodeChickens.AntiBan.performXPCheck(combatType);
		
		if(Player.isMoving() && Game.getRunEnergy() >= xCodeChickens.AntiBan.INT_TRACKER.NEXT_RUN_AT.next())
		{
			Options.setRunOn(true);
			xCodeChickens.AntiBan.INT_TRACKER.NEXT_RUN_AT.reset();
		}
	}
}
