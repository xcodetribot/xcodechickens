package scripts.xCodeChickens;

import org.tribot.api.General;
import org.tribot.api2007.ChooseOption;
import org.tribot.api2007.Game;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public abstract class Node {
	
	protected RSArea chickenArea = new RSArea(new RSTile(3014, 3298, 0), new RSTile(3020, 3282, 0));
	/**
     * Is action valid?
     * @return 
     */
    public abstract boolean isValid();
    
    /**
     * Execute action.
     */
    public abstract void execute();
    
    /**
	 * Checks if a position can be randomized by an offset, but still let it be in the area.
	 * Less ban-like, due to not always click on the right spot.
	 * @param area
	 * @param pos
	 * @param offset
	 * @author xCode
	 * @return
	 */
	protected RSTile randomizePosition(RSArea area, RSTile pos, int offset)
	{
		for(int i = 0 ; i < 10 ; i++)
		{
			RSTile randomTile = new RSTile(General.random(pos.getX() - offset , pos.getX() + offset), General.random(pos.getY() - offset, pos.getY() + offset));
			
			if(area.contains(randomTile))
				return randomTile;
		}
		return null;
	}

}
