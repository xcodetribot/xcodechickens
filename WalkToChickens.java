package scripts.xCodeChickens;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

public class WalkToChickens extends Node{

	final RSArea room1 = new RSArea(new RSTile(3021,3296,0), new RSTile(3025,3291,0));
	final RSArea room2 = new RSArea(new RSTile(3021,3290,0), new RSTile(3025,3287,0));
	final RSTile recognitionPoint = new RSTile(3027, 3287, 0),
				door1Position = new RSTile(3026, 3287, 0),
				door2Position = new RSTile(3024, 3291, 0),
				door3Position = new RSTile(3020, 3293, 0),
				chickenLocation = new RSTile(3018, 3293, 0);
	
	@Override
	public boolean isValid() {
		return !chickenArea.contains(Player.getPosition());
	}

	@Override
	public void execute() {
		if(!room1.contains(Player.getPosition()) && !room2.contains(Player.getPosition()) && recognitionPoint.distanceTo(Player.getPosition()) >= 5)
		{
			General.println("Walking to recognition point");
			if(recognitionPoint.distanceTo(Player.getPosition()) >= 4)
			{
				WebWalking.walkTo(recognitionPoint);
				General.sleep(500, 1000);
				Timing.waitCondition(new Condition() {
				
					@Override
					public boolean active() {
						return !Player.isMoving() && recognitionPoint.distanceTo(Player.getPosition()) < 3;
					}
				}, General.random(30000, 35000));
			}
		}
		if(!PathFinding.canReach(new RSTile(3025, 3287, 0), false) && recognitionPoint.distanceTo(Player.getPosition()) < 5)
		{
			General.println("Opening door 1");
			openDoor(door1Position);
		}
		else if(!PathFinding.canReach(door2Position, false))
		{
			General.println("Opening door 2");
			openDoor(door2Position);
		}
		else if(!PathFinding.canReach(door3Position, false))
		{
			General.println("Opening door 3");
			openDoor(door3Position);
		}
		else if(PathFinding.canReach(chickenLocation, false))
		{
			Walking.walkTo(chickenLocation);
		}
	}
	
	private void openDoor(RSTile tile)
	{
		if(recognitionPoint.distanceTo(Player.getPosition()) < 5)
		{
			RSObject[] doors = Objects.findNearest(10, "Door");
			for(final RSObject door : doors)
			{
				if(door.getPosition().getX() == tile.getX() && door.getPosition().getY() == tile.getY())
				{
					General.println("Found door");
					String[] actions = door.getDefinition().getActions();
					for(String action : actions)
					{
						if(action.contains("Open"))
						{
							if(DynamicClicking.clickRSObject(door, "Open"))
							{
								Timing.waitCondition(new Condition() {
									
									@Override
									public boolean active() {
										String[] actions = door.getDefinition().getActions();
										for(String action : actions)
										{
											if(action.contains("Close"))
												return true;
										}
										return false;
									}
								}, General.random(3000, 5000));
							}
						}
						General.sleep(500, 800);
						break;
					}
				}
			}
		}
	}
}
