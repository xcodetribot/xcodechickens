package scripts.xCodeChickens;

import java.util.ArrayList;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Combat;
import org.tribot.api2007.GroundItems;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;

public class PickUpItems extends Node{
	
	String[] items = null;
	
	public PickUpItems(boolean feathers, boolean bones) {
		ArrayList<String> itemList = new ArrayList<String>();
		if(feathers)
			itemList.add("Feather");
		if(bones)
			itemList.add("Bones");
		
		items = new String[itemList.size()];
		items = itemList.toArray(items);
	}

	@Override
	public boolean isValid() {
		RSGroundItem[] groundItems = GroundItems.findNearest(items);
		
		return 	groundItems.length > 0 && 
				groundItems[0].getPosition().distanceTo(Player.getPosition()) <= 5 && //Is it worth to pick it up?
				!Player.getRSPlayer().isInCombat() && !Player.isMoving() &&
				chickenArea.contains(Player.getPosition()) || Combat.getAttackingEntities().length == 0;
	}

	@Override
	public void execute() {
		
		RSGroundItem[] groundItems = GroundItems.findNearest(items);
		
		for(final RSGroundItem item : groundItems)
		{
			if (!chickenArea.contains(item.getPosition()) && item.getPosition().distanceTo(Player.getPosition()) > 6 || xCodeChickens.AntiBan.BOOL_TRACKER.USE_CLOSEST.next())
				continue;
			if(!item.isOnScreen())
				Walking.walkTo(randomizePosition(chickenArea, item.getPosition(), 1));
			
			Camera.turnToTile(item.getPosition());

			if(DynamicClicking.clickRSGroundItem(item, "Take"))
			{
				General.sleep(200,400);
				
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return !Player.isMoving();
					}
				}, General.random(5000, 6000));
			}
		}
		
	}

}
