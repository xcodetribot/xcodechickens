package scripts.xCodeChickens;

import java.awt.Point;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.ChooseOption;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

public class AttackChicken extends Node{

	@Override
	public boolean isValid() {
		RSNPC[] chickens = NPCs.findNearest("Chicken");
		for(final RSNPC chicken : chickens)
		{
			if(chicken.isInteractingWithMe() && (chicken.getHealth() <= 0 || !chicken.isValid()))
				return true;
		}
		return !Player.getRSPlayer().isInCombat() && !Player.isMoving() && chickenArea.contains(Player.getPosition()) && Combat.getAttackingEntities().length == 0;
	}

	@Override
	public void execute() {
		RSNPC[] chickens = NPCs.findNearest("Chicken");
		for(final RSNPC chicken : chickens)
		{
			if (!chickenArea.contains(chicken.getPosition()))
				continue;
			
			if(!chicken.isInCombat() && chickenArea.contains(chicken.getPosition()))
			{
				if(!chicken.isOnScreen())
					Walking.walkTo(randomizePosition(chickenArea, chicken.getPosition(), 1));
				
				final RSTile originChickenTile = chicken.getPosition();
				
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return chicken.isOnScreen() || originChickenTile != chicken.getPosition() || chicken.isInCombat();
					}
				}, General.random(2500, 3500));
				
				General.sleep(xCodeChickens.AntiBan.DELAY_TRACKER.SWITCH_OBJECT_COMBAT.next());
				if(DynamicClicking.clickRSNPC(chicken, "Attack"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							return Combat.getAttackingEntities().length > 0;
						}
					}, General.random(3000, 4000));
				}
				break;
			}
		}
	}
}
